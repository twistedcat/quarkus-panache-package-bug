# How to reproduce
- run `docker-compose up` from `./docker`
- run `mvn -U clean compile quarkus:dev` from root folder
- verify that the Postgres DB contains 2 table, including `alternativefruitentity`, generated from `org.acme.hibernate.orm.panache.otherEntity.AlternativeFruitEntity`, which exists despite the configuration line that should restrict entities to `org.acme.hibernate.orm.panache.entity`:
```
quarkus.hibernate-orm.packages=org.acme.hibernate.orm.panache.entity
```
