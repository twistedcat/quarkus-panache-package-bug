package org.acme.hibernate.orm.panache.otherEntity;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

@Entity
public class AlternativeFruitEntity extends PanacheEntity {

  @Column(length = 40, unique = true)
  public String alternativeName;
}
